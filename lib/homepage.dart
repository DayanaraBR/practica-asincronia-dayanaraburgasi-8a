import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:practica_asincronia/services/mockapi.dart';


class HomePage extends StatefulWidget {
  const HomePage ({Key? key}) : super(key: key);

    _HomePageState createState()=>_HomePageState();


}

class _HomePageState extends State<HomePage> {
  @override
  double _width=0;
  double _width1=0;
  double _width2=0;
  int counter = 0;
  int result= 0;

  void reload() {
    setState(() {
      _width=0;
    });
  }
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Pracrtica Asincronia- DB- 8voA'),
        centerTitle: true,
      ),
      body:Align(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:<Widget> [
           SizedBox(
              
                width: 100,
                height:100,
                
                child: ElevatedButton(
                
                style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green,
                ),
                onPressed: ()async{
                  var result = await MockApi().getFerrariInteger();
                  reload();
                  setState(() {
                    _width=300;
                    counter=result;
                    print('Ferrari: $result');
                    
                  });
                  
                },
                child: Icon(
                  Icons.flash_on_rounded ,
                  color: Colors.black,
                  size: 50,  
                ),
            ),  
          ),
            FutureBuilder<int>(
                future: MockApi().getFerrariInteger(),
                builder: (context, snapshot) {
                  
                  if (snapshot.hasData) {
                    return Text(
                      '${snapshot.data}',
                    style: Theme.of(context).textTheme.headline4,
                    );
                  } else if (snapshot.hasError) {
                    return Text(
                      '${snapshot.error}',
                      style: const TextStyle(fontSize: 20),
                    );
                  }
                  return Text("0",style:const TextStyle(fontSize: 40),);
                },
              ),
            AnimatedContainer(
                      duration: Duration(seconds: 1),
                      width: _width,
                      height: 20,
                      color: Colors.green,
                      margin:  const EdgeInsets.only(bottom: 10.0),
                    ) ,
            SizedBox(
                width: 100,
                height:100,
                child: ElevatedButton(
              
                style: ElevatedButton.styleFrom(
                backgroundColor: Colors.amber,
                ),
                onPressed: ()async{
                  var result = await MockApi().getHyundaiInteger();
                  
                  setState(() {
                    _width1=300;
                    counter=result;
                    print('Ferrari: $result');
                  });
                },
                child: Icon(
                  Icons.directions_bus_outlined  ,
                  color: Colors.black,
                  size: 50,
                  
                ),
            ), 
          ),  
            FutureBuilder<int>(
                future: MockApi().getFisherPriceInteger(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      '${snapshot.data}',
                    style: Theme.of(context).textTheme.headline4,
                    );
                  } else if (snapshot.hasError) {
                    return Text(
                      '${snapshot.error}',
                      style: const TextStyle(fontSize: 20),
                    );
                  }
                  return Text('0',style:const TextStyle(fontSize: 20),);
                },
              ),

            AnimatedContainer(
                      duration: Duration(seconds: 1),
                      width: _width1,
                      height: 20,
                      color: Colors.amber,
                      margin:  const EdgeInsets.only(bottom: 10.0),
                    ) ,
            SizedBox(
                width: 100,
                height:100,
                child: ElevatedButton(
              
                style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red,
                ),
                onPressed: ()async{
                  var result = await MockApi().getFisherPriceInteger();
                  
                  setState(() {
                    _width2=300;
                    counter=result;
                    print('Ferrari: $result');
                  });
                },
                child: Icon(
                  Icons.directions_run   ,
                  color: Colors.black,
                  size: 50, 
                ),
            ), 
          ),            
                    
            FutureBuilder<int>(
                future: MockApi().getFisherPriceInteger(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      '${snapshot.data}',
                    style: Theme.of(context).textTheme.headline4,
                    );
                  } else if (snapshot.hasError) {
                    return Text(
                      '${snapshot.error}',
                      style: const TextStyle(fontSize: 20),
                    );
                  }
                  return Text('0',style:const TextStyle(fontSize: 20),);
                },
              ),
            AnimatedContainer(
                      duration: Duration(seconds: 1),
                      width: _width2,
                      height: 20,
                      color: Colors.red,
                      margin:  const EdgeInsets.only(bottom: 10.0),
                    ) ,
          ],
        ),
        
      )  
    );
  }
}